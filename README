******* Midge version 0.2.41 *******
====================================

Midge, for midi generator, is a text to midi translator.
It creates type 1 (ie multitrack) midi files from text
descriptions of music.

The source language used is documented in the man page,
and demonstrated in the source files in the examples
directory.

Midge has been tested with Perl 5.8.0 on GNU/Linux, and
with ActiveState Perl 5.8.0 on Windows XP, and should
work on any system with a basic installation of Perl 5.005
or higher. The last version tested with Perl 5.005 was
version 0.2.27.

The included midi2mg script additionally requires the MIDI
modules by Sean M. Burke, which are available from CPAN.
(type `perldoc CPAN' at your prompt to find out how to
install them).

Midge is free software and you are welcome to redistribute
it under the terms of the GNU General Public Licence. Midge
comes with ABSOLUTELY NO WARRANTY.

=> Current features <=

 Sections of music can be predefined and reused multiple
  times, transposing if required.

 Allows nested loops.

 Supports setting of reverb and chorus.

 Supports setting of note on/note off velocity.

 Supports setting of individual track volume

 Supports setting of track/instrument names.

 Supports setting of key signature.

 Supports tuplets, which may be nested.

 Supports pitch bending and setting of pitch wheel range.

 Tempo and time signature can be changed within a track.

 Can choose a note or riff randomly from a list of notes and/or
  predefined riffs.

 Can choose multiple notes randomly from a list of (different length)
  notes to produce a riff of specified length.

 Can generate a sequence of notes from a user defined `chain' structure
  where for each note there is a weighted list of notes which may follow
  it.

 Can offset notes from the beat by either a specified or randomly chosen
  amount.
 
 Separate note on and note off events can be used to play simultaneous
  notes.

 Ranges (eg `8-64') can be used when setting the volume, pan, reverb,
  chorus, attack and decay, which causes a random value within the range
  to be used.

 Supports text marker events

 Supports panning events

 Supports bank changes.

 Supports rpn and nrpn controllers.
 
 An emacs mode with syntax highlighting for editing and compiling midge
  files and playing the resulting midi files (if you have a command line
  midi player). See README.elisp

=> Changes in this release <=

 New head section keyword $bar_strict to check consistency of bars,
  giving an error or warning unless each track has the same number of
  bars and numbered bars appear at the same time in each track. (Patch
  contributed by Gary Wong).
  
 Fix for timing in chords.


Installation instructions are in the file INSTALL.
Windows instructions are in README.win32

Midge's homepage is at:

=> http://www.undef.org.uk/code/midge/ <=

Please send comments, suggestions or bug reports to:

=> dave@undef.org.uk <=
