0.2.41 - Jul 17 2006

New head section keyword $bar_strict to check consistency of bars,
 giving an error or warning unless each track has the same number of
 bars and numbered bars appear at the same time in each track. (Patch
 contributed by Gary Wong).

Fix for timing in chords.

0.2.40 - Jul 16 2006

Fix for changes in the previous version that were only applied
 to input files and not standard input.

Updated the manual page.

0.2.39 - Jul 16 2006

The '|' symbol can now be used in source files to denote bars.
 Numbered bars can be noted as '|2' or '|_2'.
 The lengths of bars are not checked.

Fix for timing error when $shorten keyword is used.

Fix for midi2mg to prevent fractional note lengths.

0.2.38 - Dec 21 2003

Added a warning if the `$resolution' value is inappropriate.

Fixed a bug which prevented resolution values greater than 255 from working.

Added `$key_strict' keyword to make the notes automatically sharp or
 flat as appropriate for the key (this is still experimental). See
 examples/tutorial/key_strict.mg
 
Fixed a bug in midge and midi2mg that prevented minor keys being set correctly.

Major improvements to midge-mode.el contributed by Mario Lang, including:
 Tab completion in patch/drum/scale selection.
 Support for M-x comment-region.
 Many other internal improvements.

0.2.37 - Jul 25 2003

Fixed a bug which prevented midi2mg from writing the source file if
 the verbose option was used and it received SIGPIPE.

midi2mg now adds a `$resolution' directive to the source file to preserve
 the resolution setting of the original.

Fixed a bug which prevented midge recognising the `$resolution' directive.

0.2.36 - Jun 7 2003

Short command line options can be combined eg `midge -vo foo.mid bar.mg'

Added syntax highlighting to the emacs mode.

0.2.35 - May 10 2003

midge now adds a `reset all controllers' event at the start of all
 music tracks, which fixes playback on some external keyboards.

Added `-R' switch to prevent reset events being added.

Improved error checking for note syntax

Fixed a bug which showed up if no weightings were given for a scale
 in choose blocks (correct behaviour is to use equal weightings).

midi2mg: Improved handling of note lengths.

0.2.34 - Jan 4 2003

Extended the $patch and $bank commands to allow the LSB value of the
 bank to be specified, which is needed for external keyboards.

Fixed a bug in the %define code which was triggered by a 0 in the block

Added better error checking for note syntax.

Fixed a bug in the timing of marker events.

midi2mg: Improved parsing of tempo tracks.

0.2.33 - Oct 20 2002

Cleaned up and reorganised the source code.

Added a simpler syntax for pitch bends (see examples/tutorial/bend_simple.mg).

Added support for text events to midge and midi2mg.

Reduced timing drift during long tuplets.

midi2mg: added the `-w' switch which makes it ignore events on the
 wrong track instead of exiting.

midi2mg: a list of tracks to include or exclude can be specified
 on the command line (-n and -N switches).

midi2mg: added progress bars in verbose mode.

Added more functions to the emacs mode.
		  
0.2.32 - Jul 21 2002

Fixed the line numbers in error messages (except where `%include' is used).

Added `-I' switch for include path.

Added some include files defining drum patterns and chords.

The `l' note length option can be specified in uppercase to
 distinguish it from number 1.

Fixed a bug which stopped some notes being transposed correctly
 if note options were used.

Improved the formatting of source output from midge and midi2mg.

Added support for midi2mg to midge-mode.el

Fixed a bug in midge-mode.el which broke the midge-*-block functions.

0.2.31 - Jul 12 2002

Fixed -u option which broke in last release.

Added -U option to prevent unrolled source being saved to file.

Added $print keyword to help debugging midge source files.

Added $rpn and $nrpn keywords to adjust rpn and nrpn controllers.

midi2mg will now output to stdout if output filename is `-'
        if you really want verbose output too, put the -v first.

Added -F option to midi2mg, to prevent factorisation of time values.

0.2.30 - Apr 10 2002

Performance improved by only unrollong the tracks which
 need it instead of the whole file.

Fixed a bug which caused quoted strings to be written
 incorrectly in the unrolled source file.

midi2mg now automatically finds the midi file resolution
 and determines whether running status is used.

midi2mg now supports raw meta events and sysex events.

0.2.29 - Apr 6 2002

Added a method of inserting separate note on and note off events.
 (see example on_off.mg)

Added %verbatim/%bytes keywords for inserting raw bytes (see verbatim.mg).

Added $ctrl keyword to change any controller.

Added $pitch keyword for single pitch wheel events.

Added a decompiler script, midi2mg (requires MIDI modules from CPAN).

0.2.28 - Feb 19 2002

Minor fixes to midge-mode.el

Removed restriction on use of drum names in instrument channels.

0.2.27 - Dec 16 2001

Added support for bank select (see example bank.mg).

Added $strum keyword to allow chords to sound strummed (see strum.mg).

Fixed a bug in midge-mode.el in the patch choosing code.

0.2.26 - Oct 28 2001

Scales can now be used in %chain blocks (see example chain_scale.mg).

Fixed a timing bug with the $shorten keyword.

Fixed a bug which caused some markers to be out of time.

0.2.25 - Jun 23 2001

Fixed a bug which made some chord notes sound for too long.

0.2.24 - May 6 2001

Fixed a bug in the scale code.

Fixed a bug in the %tuplet code which accumulated rounding errors.

Added `%eval' keyword to run perl code.

Added $resolution keyword to set midi clicks per quarter note.

0.2.23 - Apr 30 2001

Notes can be added to a %choose block using built in scales.

Added --show-scale (or -S) switch to show the notes of a scale,
 or a list of supported scales.

A sequence of note lengths can be specified in a %choose block
 to allow random pitches to be used in a fixed rhythm.

Added `$unquantise' option to apply a random offset to each note.

Added --seed (or -s) switch to specify the random seed.

0.2.22 - Feb 25 2001

Added `z' and `Z' note options to allow notes to be offset
 from the beat by either a specified or randomly chosen ammount.

Added `$shorten' keyword to support the offset options.

0.2.21 - Jan 2 2001

Fixed another bug in the repeat code.

Fixed a bug in the way input is split into tokens.

0.2.20 - Dec 29 2000

Time signature can now be */{4,8,16,32,64}

Time signature can now be changed within a track.

Tempo can now be changed within a track.

All text events are now written to the tempo track.

Fixed a bug in the repeat code which was triggered by repeat
 blocks containing only rests.

unroll-loops is now set if marker events are used, or if tempo,
 time_sig, or key events are used outside the head section.

midge-mode.el now supports %tuplet blocks.

Various minor fixes to midge-mode.el

0.2.19 - Dec 1 2000

Added support for tuplets, which may be nested (see man page).

Cleaned up the code some more so that it runs under Perl's
 `strict' pragma.

Added menus to midge-mode.el although they will probably only
 work with FSF emacs (see README.elisp).

0.2.18 - Nov 25 2000

Fixed the pitch bend code so that if the maximum value is
 required the least significant bit is also used.

Cleaned up the code a little and added more comments.

midge-mode.el now uses compile-internal instead of shell-command.

0.2.17 - Oct 9 2000

Minor fixes to configure.pl

Fixed a couple of bugs in midge-mode.el  patch changing code.

0.2.16 - Jul 17 2000

Added pan_all.

Added ranges to attack, decay, volume, reverb, chorus, pan.

0.2.15 - Jul 10 2000

Fixed panning.

0.2.14 - Jul 9 2000

(elisp) Added cancel to patch/drum choose menu.

Fixed chain bug (riff length was wrong if `start' keyword was used).

Moved unroll-loops message to verbose mode.

0.2.13 - Jun 3 2000

Fixed key errors.

Added chain blocks.

0.2.12 - Mar 20 2000

Added bend range.

Corrected bend error in man page.

Added key support.

0.2.11 - Feb 27 2000

(elisp) Added patch name code.

(elisp) Added drum name code.

Added sanity check on note names.

0.2.10 - Jan 9 2000

Added marker events (demonstrated in bobby_brown.mg).

Added a few more examples.

Fixed a couple of small bugs in midge-mode.el

Cleaned up the code a little.

0.2.9 - Sep 5 1999

Fixed a bug which affected patch changes.

Fixed a bug in midge-mode.el

Cleaned up some code and error messages.

0.2.8 - Jul 12 1999

Fixed a bug in the looping code.

Added a new example and changed some others.

0.2.7 - Jul 11 1999

Rewrote the %define handling code so that %define
 blocks can contain %repeat and %bend blocks, and
 other defined riffs.

Fixed a bug in patch number handling which made
 them one patch out. Patch numbers from old source
 files will need to be increased by 1.

Fixed a bug in midge-mode.el which was triggered
 when a channel block was inserted without specifying
 an instrument name.
		 
0.2.6 - Jul 4 1999

Extended the `choose' code to generate a sequence of
 notes of specified length from a weighted list.

Added midge-mode.el emacs mode.

0.2.5 - Jun 25 1999

Added the `choose' keyword to choose randomly from a
 weighted list of notes.

[ No record was kept for older versions ]
