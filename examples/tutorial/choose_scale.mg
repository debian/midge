# This file demonstrates the scale keyword in a choose block
# and the rhythm keyword to specify note lengths to go with
# the random pitches.

@head {
	$tempo 120
	$time_sig 4/4
}

@body {
	@channel 1 "piano" {
		$patch piano_grand_ac

		# the `-' shows it's a non time-limited block, ie contains
		# a `rhythm' block. (`0' can also be used).
		%choose - {

			# c major scale, two octaves starting on c4
			scale major c4-6 [ 
			    4 2 3 2 4 3 2 3 # weightings for c4..c5
			    3 4 2 3 3 2 4   # weightings for d5..c6
			]

			# specify the note lengths
			rhythm {
				4 4 8 8 4       # lengths are specified the same `n:d'
                                # format as in the note options.

				4 4 /l8/r       # other tokens are passed through so rests
				16 3:16 8       # and predefined riffs etc can be included.

				$chorus _64     # to set a numerical value this syntax
                                # must be used to prevent the number
                                # being parsed as a note length

				8x4             # 4 eighth notes with pitches chosen separately
				/r2/16          # 2 16th notes with the same pitch
				8x3

				16 16 8         # some more notes...
				4 4 8 8

			}
		}
	}
}
