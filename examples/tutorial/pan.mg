# This file shows the use of $pan

@head {
	$time_sig 4/4
	$tempo 120
}

@body {
	@channel 1 {
		$patch guitar_jazz
		$pan 127             # pan hard right

		/l4r16/e3
	}
}
