@head {
	$tempo 120
	$time_sig 4/4
}

@body {
	@channel 1 {
		$patch cello

		# note on c5
		%verbatim {
			0    # delta time
			0x90 # note on ch1 (in hex)
			60   # note c5
			127  # velocity
		}
		/l4/r # next note starts 1/4 note later

		e4 g e

		%verbatim {
			96   # delta time (1/4 note)
			0x80 # note off ch1
			60   # note c5
			32   # velocity
		}


	}

}
