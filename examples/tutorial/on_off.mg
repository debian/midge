# This file shows how to play simultaneous notes using separate
# note-on and note-off events

@head {
	$tempo 120
	$time_sig 4/4
}

@body {
	@channel 1 {
		$patch cello
		$length 4

		+/a127/c5   # note on c5 (w/ attack 127)
		r           # c5 plays for 1/4 note before other notes start
        e4 g e      # play e4, g4, e4 over the c5
        r           # c5 plays for 1/4 note after other notes
		-/d32/c5    # note off c5 (w/ decay 32)
	}
}
