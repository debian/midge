# This file demonstrates the use of `()' to make chords.

@head {
	$tempo 120
	$time_sig 4/4
}
@body {
	%define minor { ( c e- g ) }   # define minor to be a C minor
	%define major { ( c e g ) }    # define major to be a C major
	%define 7 { ( c e g b- ) }     # define 7 to be a C 7th
	@channel 1 {
		$patch 1
		$length 2
		$octave 4

		%repeat 4 { ~major } # c major

		$patch 18
		%repeat 4 { ~minor/9/ } # a minor (C minor transposed up 9 steps)

		$patch 91
		%repeat 4 { ~major } # c major

		$patch 27
		%repeat 4 { ~7/7/ } # g 7th (C 7th transposed up 7 steps)
	}
}
