# This file shows the use of ranges to vary the pan value and
# attack (note on velocity). Ranges can be used for volume, pan
# reverb, chorus, attack and decay.

@head {
	$tempo 120
	$time_sig 4/4
}

@body {
	@channel 1 "synth" {

		$patch polysynth
		$length 8

		%repeat 8 {
			%repeat 8 { # 1 bar
				$pan 8-120            # pan randomly between 8 and 120
				$attack 80-127        # vary attack between 80 and 127
				%choose {
					1 e4 1 g4 1 a4 1 b4 1 d5 1 e6
				}
			}
		}
	}
}
