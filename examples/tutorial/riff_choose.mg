# The lead guitar part uses a `call & response' style.
# There are two `call' riffs, used alternately, and
# four `response' riffs, chosen at random each time.

@head { $tempo 120 $time_sig 4/4 }

@body {

	# lead guitar

	%define call_1 {
		/l8/d4 e r e g e /r2/r
	}

	%define call_2 {
		/l8/d4 e r e d b3 /r2/r
	}

	%define response_1 {
		/l8r2/r g4 a b- a g e
	}	

	%define response_2 {
		/l8r2/r g4 e d b3 g4 e
	}

	%define response_3 {
		/l8r2/r d4 e g a g e
	}

	%define response_4 {
		/l8r2/r b4 a g a g e
	}

	@channel 1 "lead guitar" {

		$patch 27 $volume 96

		%repeat 12 {

			~call_1
			%choose { 
				3 ~response_1
				3 ~response_2
				2 ~response_3
				2 ~response_4
			}

			~call_2
			%choose { 
				3 ~response_1
				3 ~response_2
				2 ~response_3
				2 ~response_4
			}
		}
	}

	# bass

	%define bass_1 {
		/l8r4/e2 g /r3/e r /r7/g
	}

	@channel 2 bass {

		$patch 35 $volume 127
		%repeat 12 {
			%repeat 2 { ~bass_1 }
		}
	}

	# drums

	%define drums_1 {
		/l3:8/c3 /l8/c /l3:8/d /l8/c /l4/c c /l2/d
	}

	@channel 10 drums {

		$volume 127 $reverb 32
		%repeat 12 {
			%repeat 2 { ~drums_1 }
		}
	}
}
