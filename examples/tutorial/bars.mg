@head {
	$bar_strict warn     # Print a warning for inconsistent bars.
	# $bar_strict error  # Exit with an error for inconsistent bars.

	$tempo 120
	$time_sig 4/4
}

@body {
	@channel 1 {
		$length 4
		$patch 1,1,1
		$volume 64

		|_1 c4 d e f |_2 g f e d
	}

	@channel 2 {
		$length 8
		$patch 1,1,1
		$volume 64

		|_1 c5 d e f g f e d |_2 c d e f g f e d
	}
}
