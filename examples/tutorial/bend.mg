# This file demonstrates the use of %bend and $bend_range.

@head {
	$tempo 120
	$time_sig 4/4
}

@body {
	@channel 1 guitar {
		$patch guitar_steel

		%repeat 4 {
			/l8/d4
			$bend_range 1  # set bend range to +/- 1 semitones (default is 2)
			%bend e {
				0+64  # bend up 1 semitone to f
				8-64  # back down after 1/8 note to e
				8+0   # hold for 1/8
			}
			d
			$bend_range 4  # set bend range to +/- 4 semitone
			%bend a4 {
				0+48  # bend up 3 semitones to c
				8-16  # down 1 semitone to b
				8+0   # hold for 1/8
			}
			/l8/a+3 a
		}
	}
}
