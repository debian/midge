# This example shows how to use the %tuplet keyword
#  first with a simple 3:2 tuplet, then with a nested
#  tuplet (3:2 inside of 5:3)

@head {
	$tempo 80
	$time_sig 4/4
	$resolution 120   # 3 and 5 are factors, so the tuplets divide evenly.
}

@body {

	# count out the time in 4/4
	# 1 bar repeated 7 times
	@channel 10 "click" {
		%repeat 7 {
			/l4r4/stick
		}
	}

	@channel 1 "piano" {
		/l4r4/e3 # straight bar

		# simple tuplet (1 bar played twice)
		%repeat 2 {


			e                        # 1 quarter note

			%tuplet 3:2 { f+ g f+ }  # 3 quarter notes in the space of 2

			d                        # quarter note on 4th beat of the bar

		}

		/l4r4/e # straight bar

		# nested tuplets (1 bar played twice)
		%repeat 2 {
			e                     # 1 quarter note

			%tuplet 5:3 {         # 5:3 tuplet (to end of bar (1+3=4)

				f+ g a            # 3 quarter notes (of 5) in 5:3

				%tuplet 3:2 {     # nested 3:2 tuplet (to end of 5:3
                                  #  tuplet (3+2=5))

					g f+ d        # 3 quarter notes (of 3) in 3:2

				}                 # end of 3:2 tuplet

			}                     # end of 5:3 tuplet

		}

		/l4r4/e # straight bar

	}
}
