@head {
	$tempo 80          # set initial tempo to 80 bpm
}

@body {
	@channel 1 "bass" {
		$patch bass_fg
		$length 4           # play quarter notes

		/r8/e2              # 2 bars of 4/4
		/r6/e2              # 2 bars of 3/4
	}

	@channel 2 "piano" {
		$patch piano_grand_ac
		$length 4

		$time_sig 4/4       # set time sig to 4/4 (affects all tracks)

		/r8/e6              # 2 bars

		$time_sig 3/4       # set time signature to 3/4 (affects all tracks)

		/r6/e6              # 2 bars of 3/4
	}
}
