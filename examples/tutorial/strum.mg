# this file demonstrates the $strum keyword

@head {
	$tempo 80
	$time_sig 4/4
}

@body {

	# define E and A chords
	%define E { ( e2 b e3 g+ b e4 ) }
	%define A { ( a2 e3 a c+4 e ) }

	@channel 1 "guitar" {
		$patch guitar_nylon

		$strum 4     # 4 midi clicks between each note in chords

		$length 4    # quarter notes

		%repeat 4 {

			~E r ~E ~E

			~A r ~A ~A

		}

		$strum 12    # slower strum for last chord

		$length 1

		~E

	}

}

