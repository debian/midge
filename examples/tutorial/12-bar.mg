# This file generates a simple 12 bar in A. It shows
# the use of %define and %repeat, and the use of `()'
# to play simultaneous notes.

@head {
	$tempo 140 $time_sig 4/4
	$title "twelve bar in A"
}

@body {

	# define some chords using `()'
	%define A { ( a3 c+4 e4 a4 ) }         # A major
	%define A6 { ( a3 c+4 e4 f+4 a4 ) }    # A 6th

	%define D { ~A/5/ }                    # define D chords as A chords
	%define D6 { ~A6/5/ }                  # transposed up 5 semitones

	%define E { ~A/7/ }                    # define E chords as A chords
	%define E6 { ~A6/7/ }                  # transposed up 7 semitones

	%define E7 { ( e3 g+3 b3 d4 e4 ) }     # E 7th

	# define the bass line 1 bar long
	%define bass { /l8r2/a2 /r2/c+3 /r2/e /r2/f+ } 

	@channel 1 "rhythm guitar" {

		# set volume and patch
		$volume 96 
		$patch 26 

		$length 16 # as no length is specified in the
                   # chord definitions, this default
                   # length will apply to all the chords

		%repeat 10 { # repeat the whole 12 bar 10 times

			%repeat 8 { ~A r ~A r ~A6 r ~A6 r } # 4 bars of A
			%repeat 4 { ~D r ~D r ~D6 r ~D6 r } # 2 bars of D
			%repeat 4 { ~A r ~A r ~A6 r ~A6 r } # 2 bars of A
			%repeat 2 { ~E r ~E r ~E6 r ~E6 r } # 1 bar of E
			%repeat 2 { ~D r ~D r ~D6 r ~D6 r } # 1 bar of D
			%repeat 2 { ~A r ~A r ~A6 r ~A6 r } # 1 bars of A
			~A r %repeat 7 { ~E7 r }            # 1 bar of E7
		} 
	}
	@channel 2 bass {

		# set volume and patch
		$volume 127
		$patch 34

		%repeat 10 { # repeat whole 12 bar 10 times
			%repeat 4 { ~bass }      # 4 bars of A
			%repeat 2 { ~bass/5/ }   # 2 bars transposed up to D
			%repeat 2 { ~bass }      # 2 bars of A
			~bass/7/                 # 1 bar transposed up to E
			~bass/5/                 # 1 bar transposed up to D
			%repeat 2 { ~bass }      # 2 bars of A
		}
	}
	@channel 10 drums {
		%repeat 240 { /l8/c3 c d c } # repeat simple drum pattern
                                     # all the way through
	}
	@channel 10 cymbals {
		%repeat 240 { /l8/f+3 f+ g+ f+ } # repeat simple cymbal pattern
                                         # all the way through
	}
}
