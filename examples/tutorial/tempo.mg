@head {
	$tempo 80          # set initial tempo to 80 bpm
	$time_sig 4/4      # set initial time signature to 4/4
}

@body {
	@channel 1 "bass" {
		$patch bass_fg
		$length 4           # play quarter notes

		/r16/e2             # 4 bars
	}

	@channel 2 "piano" {
		$patch piano_grand_ac
		$length 4

		/r8/e6              # 2 bars

		$tempo 120          # tempo change to 120 bpm (affects all tracks)

		/r8/e6              # 2 more bars
	}
}

