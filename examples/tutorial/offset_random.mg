# This file demonstrates how to offset notes by a random amount
# using the `Z' note option. The last bass drum should line up
# with the crash cymbal.

@head {
	$time_sig 4/4
	$tempo 80
}

@body {
	@channel 10 "drums" {

		$shorten 8                # shorten notes by 8 midi clicks
                                  # to leave space for offsets

		%repeat 2 {
			/l4r4/bd              # straight bar

			bd                    # start on the beat
			/Z-8/bd               # 2nd up to 8 clicks early
			bd                    # 3rd on the beat
			/Z+8/bd               # 4th up to 8 clicks late

			/r4/bd                # straight bar

			bd                    # start on the beat
			%repeat 3 { /Z8/bd }  # offset remaining 3 beats
                                  # by + or - 8 midi clicks 
		}
		bd                        # end on the one
	}

	@channel 10 "hi hat" {
		%repeat 8 {
			%repeat 4 { /l8/r hh_c }
		}

		/l4/cym_crash             # end on the one
	}
}
