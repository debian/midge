# The first track is completely random. There are two
# choose blocks, one choosing from an `a' scale and the
# other from a `d' scale, each within a loop which
# produces 4 bars of output. Each note has the same
# length to fix the length of the loop. See time_choose.mg
# to see how to get a fixed length output from different
# length notes.

@head { $tempo 120 $time_sig 4/4 }

@body {

	# random notes on piano
	# 4 bars of a, 4 bars of d, repeated 4 times

	@channel 1 piano {

		$length 16 $patch 1 $volume 104

		%repeat 4 {

			%repeat 64 { # 4 bars

				# pick a note from the a scale
				%choose { 
					4 a3 2 c4 3 d4 3 e4 3 g4
					2 a4 2 e5 2 d5 1 g5
				}
			}

			%repeat 64 { # 4 bars

				# pick a note from the d scale
				%choose { 
					4 d3 2 f4 3 g4 3 a4 3 c4
					2 d4 2 a5 2 g5 1 c5
				}
			}
		}

	}

	# backing chords on organ

	%define Am { ( a2 c3 e3 a3 c4 e4 ) }
	%define Dm { ( d2 f3 a3 d3 f4 a4 ) }
	

	@channel 2 organ {

		$length 1 $octave 3 $patch 18

		%repeat 4 {
			%repeat 4 { ~Am }
			%repeat 4 { ~Dm }
		}
	}
}
