# This file demonstrates the use of %pan_all to set the pan values
# of a drum kit.

@head {
	$tempo 100
	$time_sig 4/4
}

@body {
	@channel 10 "drums" {
		
		# set up pan values, 0 is hard left, 127 is hard right
		# drums not specified get the default which is either
		# the center (64) or the last value set with the $pan
		# keyword in the current track
		%pan_all {
			hh_c 56
			hh_o 56
			hh_p 56
			cym_crash 80
			cym_ride 48
			tom_l 40
			tom_h 80
			tom_hm 48
			tom_lm 72
			ftom_l 80
			ftom_h 56
		}

		$reverb 32

		# play randomly for 16 bars
		%choose 16:1 {
			1 /l8/tom_hm
			1 /l8/tom_lm
			1 /l8/tom_l
			1 /l8/tom_h
			1 /l8/ftom_h
			1 /l8/ftom_l
			1 /l8/cym_crash
			1 /l8/cym_ride
			1 /l8/hh_o
			1 /l8/hh_c
			1 /l8/hh_p
			1 /l8/bd
			1 /l8/sd_ac
			1 /l8/r

			1 /l16/tom_hm
			1 /l16/tom_lm
			1 /l16/tom_l
			1 /l16/tom_h
			1 /l16/ftom_h
			1 /l16/ftom_l
			1 /l16/cym_crash
			1 /l16/cym_ride
			1 /l16/hh_o
			1 /l16/hh_c
			1 /l16/hh_p
			1 /l16/bd
			1 /l16/sd_ac
			1 /l16/r
		}
	}
}
