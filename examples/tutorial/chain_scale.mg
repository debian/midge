# This files shows how to use a scale to specify the notes
# in a chain block, and how to add additional notes.

@head {
	$time_sig 4/4
	$tempo 120
}

@body {

	@channel 1 "piano" {
		$patch piano_grand_ac
		$volume 96

		%chain 16:1 { # improvise for 16 bars

			scale major c5 # use notes from c major scale, octave 5
                           # scale can only be used once, and must
                           # come first.

			[ # begin weighting matrix

			# to
			# c4 d4 e4 f4 g4 a4 b4 c5
			                           # from
			  0  0  1  0  4  0  2  1   # c4
			  3  0  2  0  1  0  0  0   # d4
			  2  1  0  1  0  4  0  0   # e4
			  0  0  2  0  4  0  3  0   # f4
			  4  0  0  2  1  0  0  2   # g4
			  0  0  1  0  2  0  2  1   # a4
			  0  0  0  1  2  2  0  4   # b4
			  2  0  0  0  3  0  2  0   # c5
			]

			r # add a rest to the chain

			[ 4  0  2  0  3  0  1  1  0 ] # weightings for rest->scale notes
                                          # last value is for rest->rest
                                          # each additional note will need
                                          # one extra value

			[ 2  0  3  0  2  1  0  3 ]    # weightings for scale notes->rest

			# rhythm block
			rhythm [
			         # mostly eigth notes, some quarters

                     # weighting   # value

			               4           8
			               1           4
			]
		}
	}

	@channel 2 "synth" {
		$patch organ_rock
		$volume 80
		$length 1

		%repeat 2 {

			# chord sequence | F | C | F | C | F | C | G | C |

			%repeat 3 { ( f3 a c4 f a c5 ) ( c3 e g c4 e g c5 ) }
			( g3 b d4 g b ) ( c3 e g c4 e g c5 )
		}
	}

}

