# This file demonstrates how to offset notes slightly
# using the `z' note option. See also offset_random.mg

@head {
	$time_sig 4/4
	$tempo 80
}

@body {
	@channel 10 "bass drum" {

		$shorten 5              # shorten notes by 5 midi clicks
                                # to leave space for offsets

		/l4r4/bd                # 4 straight beats
		bd /z+5/bd /r2/bd       # beat 2 played 5 midi clicks late
		/r4/bd                  # 4 straight beats
		bd /z-5/bd /r2/bd       # beat 2 played 5 midi clicks early
	}

	@channel 10 "hi hat" {
		%repeat 4 { %repeat 4 { /l8/r hh_c } }
	}
}
