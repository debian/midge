# This file demonstrates the $key_strict keyword, which makes the
# notes automatically sharp or flat as appropriate for the key.
#
# This feature is experimental and will probably not work correctly
# in all cases.

@head {
	$key_strict G   # f defaults to f sharp and octave changes at g.
                    # An octave of 3 gives a note from the same octave
                    #  that contains the regular c3.
}

@body {
	@channel 1 "piano" {
		$patch piano_grand_ac

		/l4/g3 a b c d e f g4     # G major scale (f is sharp).

		/l4/g3 a b- c d e f= g4   # G with flattened 3rd (b flat)
		                          #  and flattened 7th (f natural).

		$key B-m

		b3 c d e f g a b4         # B flat minor (b, d, e, g and a are flat).
	}
}

