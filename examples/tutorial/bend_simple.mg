@head {
	$tempo 120
	$time_sig 4/4
}

@body {
	@channel 1 "bass" {
		$patch bass_fg

		%repeat 4 {

			/l3:8/a3=>b-3=>a3 # Bend from a3 up to b-3 and back down to a3
                              # with the bend amount increasing/decreasing
                              # linearly over the duration of 3 eighth notes.
                              # By default, the bend is done over 16 steps
                              # per quarter note (giving 24 here). This can
                              # be changed using the `-b' command line switch.
		
			/l8/g3 r /l32/d /l3:32/e /l8/g a
		}
	}

	@channel 10 "tamborine" {
		%repeat 4 { /l4r4/tamb }
	}
}

