# This file demonstrates the use of the %chain block.

@head {
	$tempo 120
	$time_sig 4/4
}

@body {
	@channel 1 "guitar" {
		$patch guitar_steel

		# C major scale
		%chain 4:1 {
			start c5 # Start on c5

			c5 [ 1 e5 4 d5 ] # c5 can be followed by e5 or d5
                             # The numbers in front of the notes
                             # are weighting factors, ie d5 will
                             # be chosen more often than e5

			# set up rest of scale
			d5 [ 3 e5 1 g5 ]
			e5 [ 1 f5 ]
			f5 [ 1 g5 ]
			g5 [ 1 a5 ]
			a5 [ 2 b5 1 c5 ]
			b5 [ 1 a5 2 c6 1 c5 ]
			c6 [ 1 b5 3 c5 1 g5 ]

			# Note lengths
			rhythm [
			          1  16 # 16th notes 1/5 probability 
			          4   8 # 6th notes 4/5 probability
			]
		}
	}
}
