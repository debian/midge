# This file shows how to use a bank select, if your hardware supports it.

@head {
	$tempo 100
	$time_sig 4/4
}

@body {
	@channel 1 {
		$length 4
		$octave 3

		$marker "patch 4; no bank (should use bank 1)"
		$patch 4 /r4/e r

		$marker "patch 4; bank 2"
		$patch 2,4 /r4/e r

		$marker "patch 5; no bank (should stay on bank 2)"
		$patch 5 /r4/e r

		$marker "patch 5; bank 1"
		$bank 1 /r4/e r

		$marker "patch 5; bank 2"
		$bank 2 /r4/e r
	}

}
