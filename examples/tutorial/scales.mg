# This file shows how to define, use and transpose a riff.

@head {
	$time_sig 4/4
	$tempo 80
}

@body {

	# C major scale
	%define scale { /l8/c3 d e f g a b /r2/c4 b3 a g f e d c }

	@channel 1 {
		$patch 1 # piano

		# play scales ascending in whole steps (ie c, d, e, f#)
		~scale ~scale/2/ ~scale/4/ ~scale/6/
	}
}
