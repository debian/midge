# This is the same as note_choose.mg except different length
# notes are used in the choose blocks, with the time option
# used to fix the overall length of the blocks.

@head { $tempo 120 $time_sig 4/4 }

@body {

	# random notes on piano
	# 4 bars of a, 4 bars of d, repeated 4 times

	@channel 1 piano {

		$length 16 $patch 2 $volume 104

		%repeat 4 {

			# pick 4 bars' worth of notes from the a scale
			%choose 4:1 { 
				4 /l8/a3 2 /l16/c4 3 /l16/d4 3 /l8/e4 3 /l16/g4
				2 /l16/a4 2 /l16/e5 2 /l16/d5 1 /l8/g5 1 /l16/r
			}

			# pick 4 bars' worth of notes from the d scale
			%choose 4:1 { 
				4 /l8/d3 2 /l16/f4 3 /l16/g4 3 /l8/a4 3 /l16/c4
				2 /l16/d4 2 /l16/a5 2 /l16/g5 1 /l8/c5 1 /l16/r
			}

		}

	}

	# backing chords on organ

	%define Am { ( a2 c3 e3 a3 c4 e4 ) }
	%define Dm { ( d2 f3 a3 d3 f4 a4 ) }
	

	@channel 2 organ {

		$length 1 $octave 3 $patch 18

		%repeat 4 {
			%repeat 4 { ~Am }
			%repeat 4 { ~Dm }
		}
	}
}
