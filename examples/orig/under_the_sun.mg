# This is a reggae song using random drums and fixed bass,
# trumpet, organ and flute. It can be heard with a guitar
# part added on my mp3 page.

@head {
	$tempo 80
	$time_sig 4/4
}

@body {

	# define chords
	%define E {
		( e3 b3 e4 )
	}

	%define E_wide {
		( e3 b3 e4 b4 e5 )
	}

	%define Am {
		( a3 c4 e4 a4 )
	}

	# chords on percussive organ
	@channel 1 "rhythm" {
		$patch organ_perc
		$length 32

		%repeat 3 { # 72 bars
			%repeat 4 { # verse, 16 bars
				%repeat 3 {
					%repeat 2 { /r4/r ~Am /r3/r }
					%repeat 2 { /r4/r ~E/3/ /r3/r }
				}
				%repeat 2 { /r4/r ~E_wide/-2/ /r3/r }
				%repeat 2 { /r4/r ~E_wide /r3/r }
			}

			# chorus, 8 bars
			%repeat 3 {
				%repeat 4 { /r4/r ~E_wide/1/ /r3/r }
				%repeat 4 { /r4/r ~E_wide /r3/r }
			}
			%repeat 4 { /r4/r ~E_wide/-2/ /r3/r }
			%repeat 4 { /r4/r ~E_wide /r3/r }
		}
		$length 2 ~Am
	}

	# bass drum on beats 1 and 3 (of 4)
	@channel 10 "drums_4_on" {
		$volume 104

		%repeat 3 { # 72 bars
			%repeat 4 { # verse, 16 bars
				%repeat 8 { /l4/bd r } # 4 bars
			}
			%repeat 2 { # chorus, 8 bars
				%repeat 8 { /l4/bd r } # 4 bars
			}
		}
		$length 1 ( bd cym_crash )
	}

	# random drum on beats 2 and 4 (of 4)
	@channel 10 "drums_4_off" {
		%repeat 3 { # 72 bars
			%repeat 4 { # verse, 16 bars
				%repeat 8 { # 4 bars
					/l4/r
					%choose {
						5 tom_l 3 tom_lm 3 tom_hm 1 sd_ac
					}
				}
			}
			%repeat 2 { # chorus, 8 bars
				%repeat 7 { # 3.5 bars
					/l4/r
					%choose {
						5 tom_l 3 tom_lm 3 tom_hm 1 tom_h
					}
				}
				/l8/hh_p tom_hm tom_lm tom_l # 0.5 bars
			}
		}
	}

	# random hi hat or rest on every second eighth note
	@channel 10 "drums_8" {
		%repeat 3 { # 72 bars
			%repeat 4 { # verse, 16 bars
				%repeat 16 { # 4 bars
					/l8/r
					%choose {
						4 hh_c 1 hh_p 1 hh_o 2 r
					}
				}
			}
			%repeat 2 { # chorus, 8 bars
				%repeat 16 { # 4 bars
					/l8/r
					%choose {
						4 hh_c 1 hh_p 1 hh_o 2 r
					}
				}
			}
		}
	}

	# random drum on every second sixteenth note
	@channel 10 "drums_16" {
		$length 16

		%repeat 3 { # 72 bars
			%repeat 24 {
				%repeat 8 { # 1 bar
					r # offset the beat
					%choose {
						6 hh_c 20 r 1 tom_h 1 bongo_h
						3 conga_h_mute 2 conga_h_open
					}
				}
			}
		}
	}

	# define bass lines
	%define bass_amg { # 1 bar
		/l16/g2 /r2/a r /l8/a3 a2 /r2/g g3 d 
	}

	%define bass_de_1 { # 1 bar
		/l16/c3 /r2/d r /l8/a2 d3 e2 e3 b2 e3
	}

	%define bass_de_2 { # 2 bars
		/l16/c3 /r2/d r /l8/a2 d3 d2 d3 a2 d3 
		/l16/d3 /r2/e r /l8/b2 e3 e2 d3 c b2
	}

	%define bass_fe { # 2 bars
		%repeat 2 { /l16r2/f2 f3 r /l8/c f }
		%repeat 2 { /l16r2/e2 e3 r /l8/b2 e3 }
	}

	@channel 2 "bass" {
		$patch bass_fg
		$volume 104

		%repeat 3 { # 72 bars
			%repeat 4 { # verse, 16 bars
				%repeat 3 { ~bass_amg }
				~bass_de_1
			}

			%repeat 3 { ~bass_fe } ~bass_de_2 # 8 bars
		}
		/l1/a2
	}

	# define trumpet part all in one block
	%define trumpet_main {
		/l16r2/r /r4/a4 /l4/a /l16/g r g /r3/r
		/l16r2/r /r4/a4 c5 /l3:16/b4 /l16/g /r5/r
		/l16r2/r /r4/a4 /l4/a /l16/g r g /r3/r
		/l16r2/r /r2/d5 /r2/a4 d5 /l5:16/e /l4/r

		/l16r2/r /r2/a4 e a /r4/r /r2/g4 d g /r2/c
		/l16r2/r /r2/a4 e a r c5 /l8/b4 /l16/g /r5/r
		/l16r2/r /r2/a4 e a /r4/r /r2/g4 d g /r2/c
		/l16r2/r a4 /r6/d5 /l3:16/e /l4/r

		/l16r2/r /r2/a4 /r2/r /r4/a /l16/g /r5/r
		/l16r2/r /r2/a4 r a b c5 /l8/b4 /l16/g /r5/r
		/l16r2/r /r2/a4 /r2/r /r4/a /l16/g /r5/r
		/l16r2/r /r5/d5 /l5:16/e /l4/r

		/l16r2/r /r4/a4 /l4/a /l16/g r g /r3/r
		/l16r2/r /r4/a4 c5 /l3:16/b4 /l16/g /r5/r
		/l16r2/r /r4/a4 /l4/a /l16/g r g /r3/r
		/l16r2/r /r2/d5 /r2/a4 d5 /l5:16/e /l4/r
		
	}

	@channel 3 "trumpet" {
		$patch trumpet
		$volume 56

		~trumpet_main

		/l8:1/r # rest for chorus

		/l24:1/r # rest for guitar solo

		~trumpet_main

	}

	# define flute part all in one block
	%define flute_main {
		/l16r2/r /r2/f5 /r2/c f5 r
		/l16r2/r /r2/f5 /l8/g /l16/f /l3:16/f /l16/e /l5:16/r
		/l2/r

		/l16r2/r /r2/f5 /r2/c f5 r
		/l16r2/r /r2/f5 /l8/g /l16/f /l3:16/f /l16/e
		/l16/e /l16/f /l3:16/e /l2/r

		/l16r2/r /r2/f5 /r2/c f5 r
		/l16r2/r /r2/f5 c /l8/f /l3:16/f /l16/e /l5:16/r
		/l2/r

		/l16r2/r /r2/d5 /r2/a4 d5 r
		/l16r2/r /r2/d5 /r2/a4 d5 /l3:16/f /l16/e /l13:16/r
	}

	@channel 4 "flute" {
		$patch flute
		$volume 64

		/l16:1/r # rest for verse

		~flute_main

		/l24:1/r # rest for guitar solo

		/l16:1/r # rest for verse

		~flute_main

	}

}
