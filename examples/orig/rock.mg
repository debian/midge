# This file plays random bass and drums with chords on a synth

@head {
	$tempo 110 $time_sig 4/4
}

@body {

	%define F_SH {
		( f+3 c+4 f+4 )
	}

	%define G_SH {
		~F_SH/2/
	}

	@channel 1 "rhythm" {
		$patch 100
		$volume 64
		$length 1

		%repeat 4 {

			%repeat 4 { ~F_SH }
			%repeat 4 { ~G_SH }

		}
	}

	@channel 2 "bass" {
		$patch 37
		$volume 127

		%repeat 4 {

			/l4/f+2 # play the root note on chord change

			# random for rest of 4 bars
			%choose 3:4 {
				2 /l16/f+2 1 /l8/a2 3 /l16/a2 2 /l16/b2 1 /l16/c+3
			}

			%choose 2 {
				2 /l16/f+3 3 /l16/e3 2 /l16/a2 2 /l16/c+3
			}

			%choose 1 {
				4 /l8/f+2 3 /l16/a2 3 /l16/b2 2 /l16/c+2
				3 /l16/f+3 2 /l16/e3 1 /l16/c3
			}

			%choose 2 {
				2 /l16/f+3 3 /l16/e3 2 /l16/a2 2 /l16/c+3
			}

			%choose 1 {
				4 /l8/f+2 3 /l16/a2 3 /l16/b2 2 /l16/c+2
				3 /l16/f+3 2 /l16/e3 1 /l16/c3
			}

			/l4/g+2 # play root note on chord change

			# random for rest of 4 bars
			%choose 3:4 {
				2 /l16/g+2 1 /l8/b2 3 /l16/b2 2 /l16/c+2 1 /l16/d+3
			}

			%choose 2 {
				2 /l16/g+3 3 /l16/e3 2 /l16/b2 2 /l16/d+3
			}

			%choose 1 {
				4 /l8/g+2 3 /l16/b2 3 /l16/c+2 2 /l16/d+2
				3 /l16/g+3 2 /l16/e3 1 /l16/c3
			}

			%choose 2 {
				2 /l16/g+3 3 /l16/e3 2 /l16/b2 2 /l16/d+3
			}

			%choose 1 {
				4 /l8/g+2 3 /l16/b2 3 /l16/c+2 2 /l16/d+2
				3 /l16/g+3 2 /l16/e3 1 /l16/c+3
			}

		}
	}

	# some drum defines to be used in the %choose block
	%define HH_C {
		/l32/g+3 r
	}

	%define HH_O {
		/l32/f+3 r
	}

	%define TOM_1 {
		/l16/g3 /l16/a3
	}

	%define TOM_2 {
		/l16/a3 /l16/g3
	}

	@channel 10 "percussion" {
		%repeat 32 {
			%choose 1 {
				9 /l16/~HH_O 10 /l16/~HH_C 12 /l16/r 1 /l4/c+4
				1 /l8/~TOM_1 1 /l8/~TOM_2
			}
		}
	}

	@channel 10 "drums" {
		%repeat 8 {

			/l2/c3 # bass drum on first beat

			# random for rest of two bars
			%choose 3:2 {
				4 /l2/c3 3 /l2/d3 2 /l8/c3 1 /l8/d3
			}

			/l4/c3 # bass drum on first beat

			# random for rest of two bars
			%choose 7:4 {
				4 /l2/c3 3 /l2/d3 2 /l8/c3 1 /l8/d3
			}
		}
	}
}
