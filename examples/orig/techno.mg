# This files generates a techno song with fixed drums, bass
# and organ and a random synth part.

@head {
	$tempo 136 $time_sig 3/4
}

@body {

	# bass

	%define bass_1 { # 1 bar long
		/l8/e2 /l16r2/e /l8r2/e /l16/r e /l8r2/e r 
	}

	@channel 1 "bass" {
		$patch 35

		%repeat 16 { # 64 bars
			%repeat 2 { ~bass_1 }
			%repeat 2 { ~bass_1/-2/ } # transposed down 1 tone
		}
	}

	# cymbals

	%define cym_1 { # 4 bars
		%repeat 16 { /l8/r f+3 }
	}

	@channel 10 "cymbals" {
		$volume 80

		/l1r4/r # rest for 4 bars

		%repeat 15 { # 60 bars
			~cym_1
		}
	}

	# drums

	@channel 10 "drums" {

		/l1r8/r # rest for 8 bars

		%repeat 14 { # 56 bars
			%repeat 4 { /l4r4/c3 }
		}
	}

	# random notes on polysynth

	@channel 2 "synth" {
		$patch polysynth
		$chorus 64
		$volume 96

		/l1r16/r # rest for 16 bars

		%repeat 12 { # 48 bars
			%repeat 2 {
				%choose 1 { # 1 bar at random over E chord
					1 /l8/e4 1 /l8/g+4 3 /l16/e5 2 /l16/g5 1 /l16/a5
					1 /l16/a4 2 /l16/b4 2 /l16/d5 1 /l8/d4 2 /l16/r
				}
			}
			/l1r2/r # rest over D chord
		}
	}

	# chords on organ

	%define E {
		( e3 g+ b e4 g+ b )
	}

	@channel 3 "organ" {
		$patch 19
		$volume 48

		/l1r12/r # rest for 12 bars

		%repeat 13 {
			%repeat 2 { ~E }
			%repeat 2 { ~E/-2/  } # D chord
		}
	}
}
