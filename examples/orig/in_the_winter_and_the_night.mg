# This is a rock backing track with bass and drums, using
# %choose and %chain blocks to play drum fills and random
# cymbals. It can be heard with a guitar part on my mp3 page.

@head {
	$tempo 120
	$time_sig 4/4
}

@body {
	%define drum_1 { # 1 bar
		/l8r2/bd_ac sd_ac bd_ac /l4/sd_ac /l8/bd_ac sd_ac
	}

	%define drum_2 { # 1 bar
		/l8r2/bd_ac sd_ac /l4/bd_ac /l8/bd_ac /l4/sd_ac 
	}

	%define drum_3 { # 1 bar
		/l8r2/bd_ac sd_ac /l4/bd_ac /l8r2/sd_ac bd_ac
	}

	@channel 10 "drums" {
		$reverb 48
		$volume 127

		# 1 bar intro
		/l2r2/stick /l4r4/stick

		%repeat 8 { # 128 bars

			# 6 bars
			~drum_2 %choose { 3 ~drum_2 1 ~drum_1 }
			%repeat 4 { ~drum_2 }

			# 6 bars
			~drum_2 %choose { 3 ~drum_2 1 ~drum_1 }
			%repeat 3 { ~drum_2 } ~drum_3

			# 3 bars
			%repeat 3 { ~drum_2 }

			# 1 bar drum roll
			%chain 1 {
				start sd_ac
				bd_ac   [ 1 bd_ac 3 sd_ac 1 tom_h ]
				sd_ac   [ 1 sd_ac 2 tom_l 2 bd_ac ]
				tom_l   [ 2 tom_lm 1 tom_h 1 sd_ac ]
				tom_lm  [ 6 tom_hm 2 bd_ac ]
				tom_hm  [ 4 tom_h 1 sd_ac 2 bd_ac 1 tom_lm ]
				tom_h   [ 1 tom_l 1 tom_hm 1 bd_ac 1 sd_ac ]
				rhythm  { 8 }
			}
		}
		/l1/bd
	}

	@channel 10 "hi_hat" {
		$volume 127

		/l1r2/r # rest for intro

		%repeat 128 {
			%repeat 4 { /l8/r %choose { 4 /l8/hh_c 1 /l8/r 1 /l8/hh_p } }
		}
	}

	@channel 10 "cymbals" {
		$volume 127

		/l1r2/r # rest for intro

		%repeat 8 { # 128 bars

			# 10 bars ( EE / DD / EE / DD / EE )
			%repeat 5 {
				/l4/r %choose { 1 cym_ride 3 r 1 cym_crash } /r2/r # 1 bar
				/l4r3/r %choose { 1 cym_crash 3 r } # 1 bar
			}

			/l4/r %choose { 1 cym_crash 3 r 1 cym_ride } /r2/r # 1 bar (D)

			# 1 bar (D)
			/l8r5/r %choose { 2 cym_crash_2 3 r }
			r %choose { 2 cym_crash 3 r }

			# 4 bars (CC / BB)
			/l4/r %choose { 1 cym_crash 1 cym_chinese 3 r } /r2/r
			/l4r2/r	%choose { 1 cym_ride 3 r } r
			$length 4
			%repeat 2 {
				%choose { 1 r 3 cym_crash 1 cym_chinese } r
			}
			%choose 1 {
				3 /l8/cym_crash
				1 /l8/cym_ride
				5 /l8/r
				1 /l8/cym_splash
			}
		}
		/l1/cym_crash
	}

	%define bass_main { # 16 bars
		%repeat 2 { /l8r14/e2 e3 b2 /r13/d3 b2 d3 e }
		/l8r14/e2 e3 b2 /r14/d3 e3 d
		/l8r14/c3 g2 c4 /r14/b2 g f+
	}

	@channel 1 "bass" {
		$patch bass_fg
		$volume 127

		/l1r2/r # rest for intro

		%repeat 8 { ~bass_main } # 128 bars
		/l8/e2
	}

	%define B_5 {
		( b3 f+4 b4 )
	}

	@channel 2 "chords" {
		$volume 56
		$patch pad_sweep
		$length 1

		/r2/r # rest for count in

		%repeat 8 { # 128 bars
			%repeat 3 { ~B_5/5/ ~B_5/5/ ~B_5/3/ ~B_5/3/ }
			~B_5/1/ ~B_5/1/ ~B_5 ~B_5
		}
	}
}
